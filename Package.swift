// swift-tools-version: 5.7
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
    name: "SwiftFib",
    products: [
        .library(
            name: "SwiftFib",
            targets: ["SwiftFib", "SwiftFibFW"]),
    ],
    dependencies: [
    ],
    targets: [
        .target(
            name: "SwiftFib",
            dependencies: []
        ),
        .testTarget(
            name: "SwiftFibTests",
            dependencies: ["SwiftFib", "SwiftFibFW"]),
        .binaryTarget(
            name: "SwiftFibFW",
            url: "https://gitlab.com/examples63/swiftfib/-/raw/main/SwiftFibFW.xcframework.zip",
            checksum: "97e3b7fbbc47239cc125ab736d2ead23a4c1e50330a3030502fbc010270bba68"
        )
    ]
)
