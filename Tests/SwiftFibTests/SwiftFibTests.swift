import XCTest
@testable import SwiftFib
import SwiftFibFW


final class SwiftFibTests: XCTestCase {
    func testExample() throws {
        XCTAssertEqual(FibCounter.countFib(10), 34)
        XCTAssertEqual(FibCounter.countFib(0), 0)
        
        let counter = FibCounter()
        
        XCTAssertEqual(counter.fibonacciBinet(9), 34)
        XCTAssertEqual(counter.fibonacciBinet(0), 0)
        
        XCTAssertEqual(counter.fibonacciMemo(9), 34)
        XCTAssertEqual(counter.fibonacciMemo(0), 0)
        
//        XCTAssertTrue(counter. )
    }
}
