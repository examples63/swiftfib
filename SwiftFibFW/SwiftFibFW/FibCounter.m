//
//  FibCounter.m
//  SwiftFibFW
//
//  Created by Martin Kompan on 23/12/2022.
//

#import <Foundation/Foundation.h>
#import "FibCounter.h"

#define SQRT_5 sqrt(5)
#define PHI ((1 + SQRT_5) / 2)

@interface FibCounter ()

@property (nonatomic, strong) NSArray *knownValues;
@property (nonatomic, assign) NSUInteger count;
@property (nonatomic, strong) NSMutableDictionary *memoized;
//- (NSUInteger) fibonacciAtIndex: (NSUInteger) index;
- (BOOL) validateSelf;

@end

@implementation FibCounter

- (instancetype) init {
    self = [super init];
    
    if (self) {
        self.knownValues = @[ @0, @1, @1, @2, @3, @5, @8, @13, @21, @34, @55, @89, @144, @233, @377, @610, @987, @1597, @2584, @4181, @6765, @10946, @17711, @28657, @46368, @75025, @121393, @196418, @317811 ];
        self.count = 0;
    }
    
    return self;
}

//  Fibonacci Sequence using Binet's Fibonacci Number Formula O(log n)

- (NSUInteger) fibonacciBinet: (NSUInteger) index {
    if (index < 2) {
        self.count++;
        return index;
    }
    
    self.count++;
    // x(n) = (Phi^n - (-Phi)^-n) / √5
    long double numerator = powl(PHI, index) - powl((long double) (-1.0 * PHI), -1.0 * index);
    long double fibonacci = numerator / SQRT_5;
    
    return (unsigned long long) fibonacci;
}

//  Fibonacci Sequence using recursion with memoization

- (NSUInteger) fibonacciMemo: (NSUInteger) index {
    NSNumber *memoized = self.memoized[@(index)];
    if (memoized != nil) {
        self.count++;
        return [memoized unsignedIntegerValue];
    }
    
    if (index < 2) {
        self.count++;
        return index;
    }
    
    self.count++;
    NSUInteger fibonacci = [self fibonacciMemo: index - 1] + [self fibonacciMemo: index - 2];
    self.memoized[@(index)] = @(fibonacci);
    return fibonacci;
}

- (BOOL) validateSelf {
    __block BOOL valid = YES;
    
    [self.knownValues enumerateObjectsUsingBlock:^(id item, NSUInteger index, BOOL *stop) {
        NSUInteger test = [self fibonacciBinet: index];
        NSNumber *fibonacci = (NSNumber *) item;
        
        if ([fibonacci unsignedIntegerValue] != test) {
            NSLog(@"Confimation failed at %lu. test: %lu; expected: %@", (unsigned long)index, (unsigned long)test, fibonacci);
            valid = NO;
            *stop = YES;
        }
    }];
    
    return valid;
}

//  Fibonacci Sequence using recursion

+ (int)countFib:(int)n {
    NSMutableArray *fibSeries = [NSMutableArray new];
    
    if (n==0) return 0;
    int total = 0;
    int prev = 1;
    for (int x=1; x<n; x++){
        total = total + prev;
        prev = total - prev;
        [fibSeries addObject:[NSNumber numberWithInt:total]];
        
        
    }
    return [fibSeries[fibSeries.count - 1] intValue];
}

@end
