//
//  SwiftFibFWTests.m
//  SwiftFibFWTests
//
//  Created by Martin Kompan on 22/12/2022.
//

#import <XCTest/XCTest.h>
#import <SwiftFibFW/SwiftFibFW.h>

@interface SwiftFibFWTests : XCTestCase

@end

@implementation SwiftFibFWTests

- (void)setUp {
}

- (void)tearDown {
}

- (void)testExample {
    XCTAssertEqual([FibCounter countFib:10], 34);
    
    XCTAssertEqual([FibCounter countFib:0], 0);
    
    FibCounter *counter = [[FibCounter alloc] init];
    
    XCTAssertEqual([counter fibonacciBinet:9], 34);
    
    XCTAssertEqual([counter fibonacciBinet:0], 0);
    
    XCTAssertEqual([counter fibonacciMemo:9], 34);
    
    XCTAssertEqual([counter fibonacciMemo:0], 0);
    
    XCTAssertTrue([counter validateSelf]);
}

@end
