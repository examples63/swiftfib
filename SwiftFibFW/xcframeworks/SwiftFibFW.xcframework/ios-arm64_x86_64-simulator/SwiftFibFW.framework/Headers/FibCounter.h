//
//  FibCounter.h
//  SwiftFibFW
//
//  Created by Martin Kompan on 23/12/2022.
//

#ifndef FibCounter_h
#define FibCounter_h

@interface FibCounter :NSObject


- (NSUInteger) fibonacciMemo: (NSUInteger) index;

- (NSUInteger) fibonacciBinet: (NSUInteger) index;

- (BOOL) validateSelf;

+ (int)countFib:(int)n;

@end
#endif
